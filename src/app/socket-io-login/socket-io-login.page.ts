import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';

import { ActionsService } from '../services/actions.service';

@Component({
  selector: 'app-socket-io-login',
  templateUrl: './socket-io-login.page.html',
  styleUrls: ['./socket-io-login.page.scss'],
})
export class SocketIoLoginPage implements OnInit {

  public joinForm: FormGroup;
  public hide = true;

  constructor(
    private form: FormBuilder,
    private router: Router,
    private action: ActionsService
  ) {
    this.joinForm = this.form.group({
      username: new FormControl('', Validators.compose([])),
      password: new FormControl('123456', Validators.compose([])),
    });
  }

  ngOnInit() {
  }

  join() {
    if(!this.joinForm.value.username || !this.joinForm.value.password) {
      this.action.presentToast({ message: 'Invalid username or password', duration: 3000, closeButtonText: 'Dismiss', showCloseButton: true }); return;
    }

    // this.router.navigate(['/home/socket-io-chat']);
    this.router.navigate(['/home/socket-io-messages']);
  }

}
