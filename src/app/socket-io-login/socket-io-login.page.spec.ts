import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SocketIoLoginPage } from './socket-io-login.page';

describe('SocketIoLoginPage', () => {
  let component: SocketIoLoginPage;
  let fixture: ComponentFixture<SocketIoLoginPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SocketIoLoginPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SocketIoLoginPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
