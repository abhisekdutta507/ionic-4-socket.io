import { Injectable } from '@angular/core';
import { FeatureList } from '../interfaces/feature-list';
import * as _ from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class StoreService {

  public featureList: FeatureList;

  constructor() {
    this.featureList = this.getLocal('featureList') || {
      developmentApis: [
        { label: 'Capital Numbers Infotech Pvt. Ltd.', url: '/home/cn-development-api', isChecked: true },
        { label: 'Node JS Server - Heroku', url: '/heroku-development-api', isChecked: true },
      ],
      nativeFeatures: [
        { label: 'Barcode Scanner', url: '/home/barcode-scanner', isChecked: true },
        { label: 'Calendar', url: '/home/calendar', isChecked: true },
        { label: 'Date & Time Picker', url: '/home/date-time-picker', isChecked: true },
        { label: 'Spinner', url: '/home/spinner', isChecked: true },
        { label: 'SQLite', url: '/home/sqlite', isChecked: true },
      ],
      developmentFeatures: [
        { label: 'Alert Controller', url: '/home/alert-controller', isChecked: true },
        { label: 'Chart JS', url: '/home/chartjs', isChecked: true },
        { label: 'Socket.io Chat', url: '/home/socket-io-login', isChecked: true },
      ]
    };

    this.setLocal('featureList', JSON.stringify(this.featureList));
  }

  getCheckedFeatures() {
    return {
      developmentApis: _.filter(this.featureList.developmentApis, ['isChecked', true]),
      nativeFeatures: _.filter(this.featureList.nativeFeatures, ['isChecked', true]),
      developmentFeatures: _.filter(this.featureList.developmentFeatures, ['isChecked', true]),
    };
  }

  getLocal(i: string) {
    let item = localStorage.getItem(i);
    return item ? JSON.parse(item) : false;
  }

  setLocal(i: string, v: string) {
    localStorage.setItem(i, v);
  }
}
