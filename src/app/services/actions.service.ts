import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

import { Toast } from '@ionic-native/toast/ngx';

@Injectable({
  providedIn: 'root'
})
export class ActionsService {

  private toast: any;

  constructor(
    private Toast: ToastController,
    private NativeToast: Toast,
  ) { }

  async presentToast(param: any) {
    this.toast = await this.Toast.create(
      Object.assign({position: 'bottom', cssClass: 'toast', mode: 'ios', color: 'dark'}, param)
    );
    this.toast.present();
  }

  async presentNativeToast(params: any) {
    this.toast = await this.NativeToast.showWithOptions(
      Object.assign({position: 'bottom', addPixelsY: -150, duration: 1000}, params)
    ).toPromise();
  }
}
