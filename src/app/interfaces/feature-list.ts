export interface FeatureList {
  developmentApis?: Array<[]>,
  nativeFeatures?: Array<[]>,
  developmentFeatures?: Array<[]>
}
