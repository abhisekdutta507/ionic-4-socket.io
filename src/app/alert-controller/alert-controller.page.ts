import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-alert-controller',
  templateUrl: './alert-controller.page.html',
  styleUrls: ['./alert-controller.page.scss'],
})
export class AlertControllerPage implements OnInit {

  private alert: any;

  constructor(
    public alertController: AlertController
  ) { }

  ngOnInit() {
  }

  async presentAlert() {
    this.alert = await this.alertController.create({
      header: 'Header',
      // subHeader: 'Sub header',
      // message: 'This is an alert message.',
      inputs: [
        // {
        //   name: 'brandName',
        //   type: 'text',
        //   label: 'Brand name'
        // },
        {
          name: 'car',
          type: 'radio',
          label: 'Marcedes',
          value: 'Marcedes'
        },
        {
          name: 'car',
          type: 'radio',
          label: 'BMW',
          value: 'BMW'
        }
      ],
      buttons: ['Done'],
      mode: 'ios'
    });

    await this.alert.present();
  }

}
