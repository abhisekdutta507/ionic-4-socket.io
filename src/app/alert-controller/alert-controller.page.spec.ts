import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlertControllerPage } from './alert-controller.page';

describe('AlertControllerPage', () => {
  let component: AlertControllerPage;
  let fixture: ComponentFixture<AlertControllerPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlertControllerPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlertControllerPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
