import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SocketIoChatPage } from './socket-io-chat.page';

const routes: Routes = [
  {
    path: '',
    component: SocketIoChatPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SocketIoChatPage]
})
export class SocketIoChatPageModule {}
