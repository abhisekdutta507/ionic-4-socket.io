import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SocketIoRegisterPage } from './socket-io-register.page';

const routes: Routes = [
  {
    path: '',
    component: SocketIoRegisterPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SocketIoRegisterPage]
})
export class SocketIoRegisterPageModule {}
