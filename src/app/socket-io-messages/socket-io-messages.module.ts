import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SocketIoMessagesPage } from './socket-io-messages.page';

const routes: Routes = [
  {
    path: '',
    component: SocketIoMessagesPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SocketIoMessagesPage]
})
export class SocketIoMessagesPageModule {}
