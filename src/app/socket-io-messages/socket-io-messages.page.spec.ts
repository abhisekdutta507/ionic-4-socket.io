import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SocketIoMessagesPage } from './socket-io-messages.page';

describe('SocketIoMessagesPage', () => {
  let component: SocketIoMessagesPage;
  let fixture: ComponentFixture<SocketIoMessagesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SocketIoMessagesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SocketIoMessagesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
