import { Component, OnInit, ViewChild } from '@angular/core';
import { IonInfiniteScroll } from '@ionic/angular';

@Component({
  selector: 'app-socket-io-messages',
  templateUrl: './socket-io-messages.page.html',
  styleUrls: ['./socket-io-messages.page.scss'],
})
export class SocketIoMessagesPage implements OnInit {
  @ViewChild(IonInfiniteScroll, {static: true}) infiniteScroll: IonInfiniteScroll;

  public messages = [
    {
      recipient: true,
      text: 'In lobortis velit ut auctor ultricies. In lobortis velit ut auctor ultricies. In lobortis velit ut auctor ultricies.'
    },
    {
      recipient: false,
      text: 'Aenean sed elit pulvinar arcu.'
    },
    {
      recipient: true,
      text: 'Pellentesque tincidunt urna quis mi convallis blandit.'
    },
    {
      recipient: false,
      text: 'Aenean sed elit pulvinar arcu.'
    },
    {
      recipient: true,
      text: 'Pellentesque tincidunt urna quis mi convallis blandit.'
    },
    {
      recipient: false,
      text: 'Aenean sed elit pulvinar arcu.'
    },
    {
      recipient: true,
      text: 'Pellentesque tincidunt urna quis mi convallis blandit.'
    },
    {
      recipient: false,
      text: 'Aenean sed elit pulvinar arcu.'
    },
    {
      recipient: true,
      text: 'Pellentesque tincidunt urna quis mi convallis blandit.'
    }
  ];

  constructor() { }

  ngOnInit() {
  }

  loadData(event: any) {
    setTimeout(() => {
      event.target.complete();

      // App logic to determine if all data is loaded
      // and disable the infinite scroll
      // event.target.disabled = true;
    }, 2000);
  }

}
