import { Component, OnInit } from '@angular/core';

import { FeatureList } from '../interfaces/feature-list';

import { ActionsService } from '../services/actions.service';
import { StoreService } from '../services/store.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {

  public featureList: FeatureList;

  constructor(
    public store: StoreService,
    private action: ActionsService
  ) {
    this.featureList = this.store.featureList;
  }

  ngOnInit() {
  }

  modifyFeatures() {
    this.store.setLocal('featureList', JSON.stringify(this.featureList));
    this.action.presentToast({ message: 'Modified display features!', duration: 3000, closeButtonText: 'Dismiss', showCloseButton: true });
  }

}
