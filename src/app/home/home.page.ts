import { Component } from '@angular/core';

import { FeatureList } from '../interfaces/feature-list';

import { StoreService } from '../services/store.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  public featureList: FeatureList;

  constructor(
    public store: StoreService
  ) {
    this.featureList = this.store.getCheckedFeatures();
  }

  ionViewWillEnter() {
  }

}
