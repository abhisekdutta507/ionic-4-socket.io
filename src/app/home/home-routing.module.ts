import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomePage } from './home.page';

const routes: Routes = [
  { path: '', pathMatch: 'full', component: HomePage },
  { path: 'socket-io-login', loadChildren: () => import('../socket-io-login/socket-io-login.module').then(m => m.SocketIoLoginPageModule) },
  { path: 'socket-io-register', loadChildren: () => import('../socket-io-register/socket-io-register.module').then(m => m.SocketIoRegisterPageModule) },
  { path: 'socket-io-chat', loadChildren: () => import('../socket-io-chat/socket-io-chat.module').then(m => m.SocketIoChatPageModule) },
  { path: 'socket-io-messages', loadChildren: () => import('../socket-io-messages/socket-io-messages.module').then(m => m.SocketIoMessagesPageModule) },
  
  { path: 'notifications', loadChildren: () => import('../notifications/notifications.module').then(m => m.NotificationsPageModule) },

  { path: 'alert-controller', loadChildren: () => import('../alert-controller/alert-controller.module').then(m => m.AlertControllerPageModule) }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule {}